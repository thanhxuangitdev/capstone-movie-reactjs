import React from "react";

import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { postLogin } from "./../../service/userService";
import { Button, Form, Input, message } from "antd";
import { setUserInfo } from "../../redux-toolkit/userSlice";
import { userLocalService } from "../../service/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/93603-loading-lottie-animation.json";
import {
  setUserAction,
  setUserActionService,
} from "../../redux/action/userAction";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    console.log("Success:", values);
    postLogin(values)
      .then((res) => {
        console.log(res);
        message.success("Dang nhap thanh cong");
        dispatch(setUserInfo(res.data.content));
        userLocalService.set(res.data.content);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Dang nhap that bai");
        console.log(err);
      });
  };
  const onFinishReduxThunk = (values) => {
    const handleNavigate = () => {
      setTimeout(() => {
        navigate("/");
      }, 1000);
    };
    dispatch(setUserActionService(values, handleNavigate));
  };

  const onFinishFailed = (errorInfo) => {};
  return (
    <div className="w-screen h-screen flex justify-center items-center bg-blue-500">
      <div className="container p-5 rounded bg-white flex">
        <div className="w-1/2">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div className="w-1/2 pt-28">
          <Form
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                span: 24,
              }}
              className="text-center"
            >
              <Button
                className="bg-blue-500 hover:text-white"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
