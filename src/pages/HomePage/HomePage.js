import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { getMovieList } from "./../../service/movieService";
import MovieList from "./MovieList/MovieList";
import MovieTab from "./MovieTab/MovieTab";

export default function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    getMovieList()
      .then((res) => {
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div>
      <div className="container mx-auto">
        <MovieList movieArr={movieArr} />
        <MovieTab />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    </div>
  );
}
