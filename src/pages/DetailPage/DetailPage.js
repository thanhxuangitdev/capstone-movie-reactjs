import React from "react";
import { useParams } from "react-router-dom";

export default function DetailPage() {
  let params = useParams();
  console.log("🚀 ~ file: DetailPage.js:6 ~ DetailPage ~ params", params);

  return (
    <div>
      <h2 className="text-red-600 text-center font-black">
        Ma phim: {params.id}
      </h2>
    </div>
  );
}
