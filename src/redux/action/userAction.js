import { SET_USER_LOGIN } from "./../constant/userContant";
import { postLogin } from "./../../service/userService";
import { message } from "antd";

export const setUserAction = (payload) => {
  return {
    type: SET_USER_LOGIN,
    payload,
  };
};
export const setUserActionService = (values, onSuccess) => {
  return (dispatch) => {
    postLogin(values)
      .then((res) => {
        message.success("Dang nhap thanh cong");
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error("Dang nhap that bai");
      });
  };
};
